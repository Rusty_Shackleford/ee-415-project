`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/14/2020 08:37:08 PM
// Design Name: 
// Module Name: Four_Way_Adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Four_Way_Adder #(parameter p_BIT_WIDTH = 32)(
    input i_CLK,
    input wire signed [p_BIT_WIDTH-1:0] i_A,
    input wire signed [p_BIT_WIDTH-1:0] i_B,
    input wire signed [p_BIT_WIDTH-1:0] i_C,
    input wire signed [p_BIT_WIDTH-1:0] i_D,
    output reg signed [p_BIT_WIDTH+1:0] o_S
    );
    
    always@(posedge i_CLK)
    begin
        o_S <= i_A + i_B + i_C + i_D;
    end
endmodule
