
module PIPO #(parameter p_BIT_WIDTH = 35) (
	input wire i_CLK,
	input wire signed [p_BIT_WIDTH:0] i_INPUT,
	output reg signed [p_BIT_WIDTH:0] o_OUTPUT
);

always@(posedge i_CLK)
begin
	o_OUTPUT <= i_INPUT;
end
endmodule
