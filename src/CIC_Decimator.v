module CIC_Decimator #(parameter p_FILTER_ORDER = 7,parameter p_BIT_WIDTH = 35, p_DECIMATION_VALUE = 64) (
	input wire i_CLK,
	input wire signed [p_BIT_WIDTH:0] i_INPUT_SAMPLE,
	output reg o_SAMPLE_READY,
	output wire signed [p_BIT_WIDTH:0] o_DECIMATED_SAMPLE
);


reg [6:0] r_DECIMATION_COUNTER;
reg r_DECIMATION_ENABLE;
wire signed [p_BIT_WIDTH:0] w_INT_DELAY_LINE [p_FILTER_ORDER-1:0];
wire signed [p_BIT_WIDTH:0] w_COMB_DELAY_LINE [p_FILTER_ORDER-1:0];

always@(posedge i_CLK) begin:DECIMATOR_ENABLE
	if(r_DECIMATION_COUNTER == p_DECIMATION_VALUE-1)
	begin
		r_DECIMATION_ENABLE <= 1'b1;
		r_DECIMATION_COUNTER <= 7'd0;
		o_SAMPLE_READY <= 1;
	end
	else
	begin
		r_DECIMATION_ENABLE <= 1'b0;
		r_DECIMATION_COUNTER <= r_DECIMATION_COUNTER + 1'b1;
		o_SAMPLE_READY <= 0;
	end
end


//generate if here
genvar i;
generate
	if(p_FILTER_ORDER == 1)
	begin
		CIC_Integrator #(.p_BIT_WIDTH(p_BIT_WIDTH)) first_stage_integrator (
			.i_CLK(i_CLK),
			.i_INPUT_SAMPLE(i_INPUT_SAMPLE),
			.o_OUTPUT_SAMPLE(w_INT_DELAY_LINE[0])
		);
	end
	else
	begin
		CIC_Integrator #(.p_BIT_WIDTH(p_BIT_WIDTH)) first_stage_integrator (
			.i_CLK(i_CLK),
			.i_INPUT_SAMPLE(i_INPUT_SAMPLE),
			.o_OUTPUT_SAMPLE(w_INT_DELAY_LINE[0])
		);
		for(i = 1; i < p_FILTER_ORDER; i = i +1)
		begin
		CIC_Integrator #(.p_BIT_WIDTH(p_BIT_WIDTH)) stage_x (
			.i_CLK(i_CLK),
			.i_INPUT_SAMPLE(w_INT_DELAY_LINE[i-1]),
			.o_OUTPUT_SAMPLE(w_INT_DELAY_LINE[i])
		);
		end
	end

endgenerate

//buffer here

generate
	if(p_FILTER_ORDER == 1)
	begin
		PIPO #(.p_BIT_WIDTH(p_BIT_WIDTH)) buffer (
			.i_CLK(i_CLK),
			.i_INPUT(w_INT_DELAY_LINE[0]),
			.o_OUTPUT(w_COMB_DELAY_LINE[0])
		);
	end
	else
	begin
		PIPO #(.p_BIT_WIDTH(p_BIT_WIDTH)) buffer (
			.i_CLK(i_CLK),
			.i_INPUT(w_INT_DELAY_LINE[p_FILTER_ORDER-1]),
			.o_OUTPUT(w_COMB_DELAY_LINE[0])
		);
	end
endgenerate


//generate if here

generate
	if(p_FILTER_ORDER == 1)
	begin
		CIC_Comb #(.p_BIT_WIDTH(p_BIT_WIDTH)) first_stage_comb(
			.i_CLK(i_CLK),
			.i_DOWNSAMPLE_ENABLE(r_DECIMATION_ENABLE),
			.i_INPUT_SAMPLE(w_COMB_DELAY_LINE[0]),
			.o_OUTPUT_SAMPLE(o_DECIMATED_SAMPLE)
		);
	end
	else
	begin
		for(i = 0; i < p_FILTER_ORDER-1; i = i + 1)
		begin
		CIC_Comb #(.p_BIT_WIDTH(p_BIT_WIDTH)) stage_y (
			.i_CLK(i_CLK),
			.i_DOWNSAMPLE_ENABLE(r_DECIMATION_ENABLE),
			.i_INPUT_SAMPLE(w_COMB_DELAY_LINE[i]),
			.o_OUTPUT_SAMPLE(w_COMB_DELAY_LINE[i+1])
		);
		end
		CIC_Comb #(.p_BIT_WIDTH(p_BIT_WIDTH)) stage_y (
			.i_CLK(i_CLK),
			.i_DOWNSAMPLE_ENABLE(r_DECIMATION_ENABLE),
			.i_INPUT_SAMPLE(w_COMB_DELAY_LINE[p_FILTER_ORDER-1]),
			.o_OUTPUT_SAMPLE(o_DECIMATED_SAMPLE)
		);
	end
endgenerate

endmodule
