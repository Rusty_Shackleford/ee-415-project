//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Feb 21 16:14:57 2020
//Host        : Necryotiks running 64-bit Antergos Linux
//Command     : generate_target PDM_to_PCM_Base.bd
//Design      : PDM_to_PCM_Base
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "PDM_to_PCM_Base,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=PDM_to_PCM_Base,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=13,numReposBlks=13,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=13,numPkgbdBlks=0,bdsource=USER,da_clkrst_cnt=4,synth_mode=Global}" *) (* HW_HANDOFF = "PDM_to_PCM_Base.hwdef" *) 
module PDM_to_PCM_Base
   (i_CLK,
    i_CTRL_BITSTREAM,
    i_INPUT_0,
    i_INPUT_1,
    i_INPUT_2,
    i_INPUT_3);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.I_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.I_CLK, CLK_DOMAIN PDM_to_PCM_Base_i_CLK, FREQ_HZ 3072000, INSERT_VIP 0, PHASE 0.000" *) input i_CLK;
  input [19:0]i_CTRL_BITSTREAM;
  input i_INPUT_0;
  input i_INPUT_1;
  input i_INPUT_2;
  input i_INPUT_3;

  wire [31:0]CIC_Decimator_0_o_DECIMATED_SAMPLE;
  wire [31:0]CIC_Decimator_1_o_DECIMATED_SAMPLE;
  wire [31:0]CIC_Decimator_2_o_DECIMATED_SAMPLE;
  wire [31:0]CIC_Decimator_3_o_DECIMATED_SAMPLE;
  wire [31:0]SIPO_0_o_OUTPUT;
  wire [31:0]SIPO_1_o_OUTPUT;
  wire [31:0]SIPO_2_o_OUTPUT;
  wire [31:0]SIPO_3_o_OUTPUT;
  wire Shift_Register_Shim_0_o_OUTPUT;
  wire Shift_Register_Shim_1_o_OUTPUT;
  wire Shift_Register_Shim_2_o_OUTPUT;
  wire Shift_Register_Shim_3_o_OUTPUT;
  wire i_CLK_1;
  wire [19:0]i_CTRL_BITSTREAM;
  wire i_INPUT_0_1;
  wire i_INPUT_1_1;
  wire i_INPUT_2_1;
  wire i_INPUT_3_1;

  assign i_CLK_1 = i_CLK;
  assign i_INPUT_0_1 = i_INPUT_0;
  assign i_INPUT_1_1 = i_INPUT_1;
  assign i_INPUT_2_1 = i_INPUT_2;
  assign i_INPUT_3_1 = i_INPUT_3;
  PDM_to_PCM_Base_CIC_Decimator_0_0 CIC_Decimator_0
       (.i_CLK(i_CLK_1),
        .i_INPUT_SAMPLE(SIPO_0_o_OUTPUT),
        .o_DECIMATED_SAMPLE(CIC_Decimator_0_o_DECIMATED_SAMPLE));
  PDM_to_PCM_Base_CIC_Decimator_0_1 CIC_Decimator_1
       (.i_CLK(i_CLK_1),
        .i_INPUT_SAMPLE(SIPO_1_o_OUTPUT),
        .o_DECIMATED_SAMPLE(CIC_Decimator_1_o_DECIMATED_SAMPLE));
  PDM_to_PCM_Base_CIC_Decimator_0_2 CIC_Decimator_2
       (.i_CLK(i_CLK_1),
        .i_INPUT_SAMPLE(SIPO_2_o_OUTPUT),
        .o_DECIMATED_SAMPLE(CIC_Decimator_2_o_DECIMATED_SAMPLE));
  PDM_to_PCM_Base_CIC_Decimator_0_3 CIC_Decimator_3
       (.i_CLK(i_CLK_1),
        .i_INPUT_SAMPLE(SIPO_3_o_OUTPUT),
        .o_DECIMATED_SAMPLE(CIC_Decimator_3_o_DECIMATED_SAMPLE));
  PDM_to_PCM_Base_Four_Way_Adder_0_0 Four_Way_Adder_0
       (.i_A(CIC_Decimator_0_o_DECIMATED_SAMPLE),
        .i_B(CIC_Decimator_1_o_DECIMATED_SAMPLE),
        .i_C(CIC_Decimator_2_o_DECIMATED_SAMPLE),
        .i_CLK(i_CLK_1),
        .i_D(CIC_Decimator_3_o_DECIMATED_SAMPLE));
  PDM_to_PCM_Base_SIPO_0_0 SIPO_0
       (.i_CLK(i_CLK_1),
        .i_INPUT(Shift_Register_Shim_0_o_OUTPUT),
        .o_OUTPUT(SIPO_0_o_OUTPUT));
  PDM_to_PCM_Base_SIPO_0_1 SIPO_1
       (.i_CLK(i_CLK_1),
        .i_INPUT(Shift_Register_Shim_1_o_OUTPUT),
        .o_OUTPUT(SIPO_1_o_OUTPUT));
  PDM_to_PCM_Base_SIPO_0_2 SIPO_2
       (.i_CLK(i_CLK_1),
        .i_INPUT(Shift_Register_Shim_2_o_OUTPUT),
        .o_OUTPUT(SIPO_2_o_OUTPUT));
  PDM_to_PCM_Base_SIPO_0_3 SIPO_3
       (.i_CLK(i_CLK_1),
        .i_INPUT(Shift_Register_Shim_3_o_OUTPUT),
        .o_OUTPUT(SIPO_3_o_OUTPUT));
  PDM_to_PCM_Base_Shift_Register_Shim_0_0 Shift_Register_Shim_0
       (.i_CLK(i_CLK_1),
        .i_CTRL_BITSTREAM(i_CTRL_BITSTREAM),
        .i_INPUT(i_INPUT_0_1),
        .o_OUTPUT(Shift_Register_Shim_0_o_OUTPUT));
  PDM_to_PCM_Base_Shift_Register_Shim_0_1 Shift_Register_Shim_1
       (.i_CLK(i_CLK_1),
        .i_CTRL_BITSTREAM({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .i_INPUT(i_INPUT_1_1),
        .o_OUTPUT(Shift_Register_Shim_1_o_OUTPUT));
  PDM_to_PCM_Base_Shift_Register_Shim_0_2 Shift_Register_Shim_2
       (.i_CLK(i_CLK_1),
        .i_CTRL_BITSTREAM({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .i_INPUT(i_INPUT_2_1),
        .o_OUTPUT(Shift_Register_Shim_2_o_OUTPUT));
  PDM_to_PCM_Base_Shift_Register_Shim_0_3 Shift_Register_Shim_3
       (.i_CLK(i_CLK_1),
        .i_CTRL_BITSTREAM({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .i_INPUT(i_INPUT_3_1),
        .o_OUTPUT(Shift_Register_Shim_3_o_OUTPUT));
endmodule
