
module CIC_Comb #(parameter p_BIT_WIDTH = 35) (
	input wire i_CLK,
	input wire i_DOWNSAMPLE_ENABLE,
	input wire signed [p_BIT_WIDTH:0] i_INPUT_SAMPLE,
	output reg signed [p_BIT_WIDTH:0] o_OUTPUT_SAMPLE
);

reg signed [p_BIT_WIDTH:0] r_DELAY_REG;

always@(posedge i_CLK)
begin
	if(i_DOWNSAMPLE_ENABLE)
	begin
		o_OUTPUT_SAMPLE <= i_INPUT_SAMPLE - r_DELAY_REG;
		r_DELAY_REG <= i_INPUT_SAMPLE;
	end
	else
	begin
		o_OUTPUT_SAMPLE <= o_OUTPUT_SAMPLE;
		r_DELAY_REG <= r_DELAY_REG;
	end
end
endmodule
