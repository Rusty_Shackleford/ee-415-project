module CIC_Integrator #(parameter p_BIT_WIDTH = 35) (
	input wire i_CLK,
	input wire signed [p_BIT_WIDTH:0] i_INPUT_SAMPLE,
	output reg signed [p_BIT_WIDTH:0] o_OUTPUT_SAMPLE
);

//NOTE: MSB is dedicated to sign bit
reg signed [p_BIT_WIDTH:0] r_DELAY_REG;

always@(posedge i_CLK)
begin
	o_OUTPUT_SAMPLE <= i_INPUT_SAMPLE + r_DELAY_REG;
	r_DELAY_REG <= o_OUTPUT_SAMPLE;
end

endmodule
