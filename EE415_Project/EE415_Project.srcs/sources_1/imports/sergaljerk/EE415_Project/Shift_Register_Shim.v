
module Shift_Register_Shim  (
	input wire i_CLK,
	input wire i_INPUT,
	input wire [19:0] i_CTRL_BITSTREAM,
	output wire o_OUTPUT
);

wire [3:0] w_INTERCONNECT;

SRLC32E srl_0 (
.A(i_CTRL_BITSTREAM[4:0]),
.CE(1'b1),
.CLK(i_CLK),
.D(i_INPUT),
.Q31(),
.Q(w_INTERCONNECT[0])
);

SRLC32E srl_1 (
.A(i_CTRL_BITSTREAM[9:5]),
.CE(1'b1),
.CLK(i_CLK),
.D(w_INTERCONNECT[0]),
.Q31(),
.Q(w_INTERCONNECT[1])
);

SRLC32E srl_2 (
.A(i_CTRL_BITSTREAM[14:10]),
.CE(1'b1),
.CLK(i_CLK),
.D(w_INTERCONNECT[1]),
.Q31(),
.Q(w_INTERCONNECT[2])
);

SRLC32E srl_3 (
.A(i_CTRL_BITSTREAM[19:15]),
.CE(1'b1),
.CLK(i_CLK),
.D(w_INTERCONNECT[2]),
.Q31(),
.Q(w_INTERCONNECT[3])
);

assign o_OUTPUT = w_INTERCONNECT[3];
endmodule
