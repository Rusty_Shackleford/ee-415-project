//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Feb 21 16:14:57 2020
//Host        : Necryotiks running 64-bit Antergos Linux
//Command     : generate_target PDM_to_PCM_Base_wrapper.bd
//Design      : PDM_to_PCM_Base_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module PDM_to_PCM_Base_wrapper
   (i_CLK,
    i_CTRL_BITSTREAM,
    i_INPUT_0,
    i_INPUT_1,
    i_INPUT_2,
    i_INPUT_3);
  input i_CLK;
  input [19:0]i_CTRL_BITSTREAM;
  input i_INPUT_0;
  input i_INPUT_1;
  input i_INPUT_2;
  input i_INPUT_3;

  wire i_CLK;
  wire [19:0]i_CTRL_BITSTREAM;
  wire i_INPUT_0;
  wire i_INPUT_1;
  wire i_INPUT_2;
  wire i_INPUT_3;

  PDM_to_PCM_Base PDM_to_PCM_Base_i
       (.i_CLK(i_CLK),
        .i_CTRL_BITSTREAM(i_CTRL_BITSTREAM),
        .i_INPUT_0(i_INPUT_0),
        .i_INPUT_1(i_INPUT_1),
        .i_INPUT_2(i_INPUT_2),
        .i_INPUT_3(i_INPUT_3));
endmodule
