# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "p_CIC_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "p_MSB" -parent ${Page_0}
  ipgui::add_param $IPINST -name "p_SRL_LENGTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.p_CIC_WIDTH { PARAM_VALUE.p_CIC_WIDTH } {
	# Procedure called to update p_CIC_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.p_CIC_WIDTH { PARAM_VALUE.p_CIC_WIDTH } {
	# Procedure called to validate p_CIC_WIDTH
	return true
}

proc update_PARAM_VALUE.p_MSB { PARAM_VALUE.p_MSB } {
	# Procedure called to update p_MSB when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.p_MSB { PARAM_VALUE.p_MSB } {
	# Procedure called to validate p_MSB
	return true
}

proc update_PARAM_VALUE.p_SRL_LENGTH { PARAM_VALUE.p_SRL_LENGTH } {
	# Procedure called to update p_SRL_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.p_SRL_LENGTH { PARAM_VALUE.p_SRL_LENGTH } {
	# Procedure called to validate p_SRL_LENGTH
	return true
}


proc update_MODELPARAM_VALUE.p_CIC_WIDTH { MODELPARAM_VALUE.p_CIC_WIDTH PARAM_VALUE.p_CIC_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.p_CIC_WIDTH}] ${MODELPARAM_VALUE.p_CIC_WIDTH}
}

proc update_MODELPARAM_VALUE.p_SRL_LENGTH { MODELPARAM_VALUE.p_SRL_LENGTH PARAM_VALUE.p_SRL_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.p_SRL_LENGTH}] ${MODELPARAM_VALUE.p_SRL_LENGTH}
}

proc update_MODELPARAM_VALUE.p_MSB { MODELPARAM_VALUE.p_MSB PARAM_VALUE.p_MSB } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.p_MSB}] ${MODELPARAM_VALUE.p_MSB}
}

