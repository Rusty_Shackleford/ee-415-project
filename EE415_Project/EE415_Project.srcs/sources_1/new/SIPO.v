`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/19/2020 05:50:06 PM
// Design Name: 
// Module Name: SIPO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SIPO
#(parameter p_CIC_WIDTH = 32, parameter p_SRL_LENGTH = 843, parameter p_MSB = 31) (
	input wire i_CLK,
	input wire i_INPUT,
	output reg signed [p_CIC_WIDTH-1:0] o_OUTPUT
);

//This should infer a variable length SRL
localparam p_LSB = p_MSB - p_CIC_WIDTH + 1;
reg [p_SRL_LENGTH-1:0] r_SRL;
always@(posedge i_CLK)
begin
	r_SRL[0] <= i_INPUT;
	r_SRL <= r_SRL << 1'b1;
	o_OUTPUT <= r_SRL[p_MSB:p_LSB];
end


endmodule
