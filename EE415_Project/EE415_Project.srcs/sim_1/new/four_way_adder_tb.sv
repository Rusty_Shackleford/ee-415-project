`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/21/2020 03:48:41 PM
// Design Name: 
// Module Name: four_way_adder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module four_way_adder_tb();


    Four_Way_Adder uut (
	    .i_CLK(r_CLK),
	    .i_A(r_A),
	    .i_B(r_B),
	    .i_C(r_C),
	    .i_D(r_D),
	    .o_S(w_S)
    );

    logic [31:0] r_A = 0;
    logic [31:0] r_B = 0;
    logic [31:0] r_C = 0;
    logic [31:0] r_D = 0;
    logic r_CLK;
    logic [33:0] w_S;

    always
	    #20 r_CLK = ~r_CLK;

    integer i;
    initial begin
	    r_CLK = 1'b0;
	    r_A = 32'd0;
	    r_B = 32'd0;
	    r_C = 32'd0;
	    r_D = 32'd0;
	    for(i = 0; i < 8; i = i + 1)
	    begin
		    # 20 r_A = $random();
		    r_B = $random();
		    r_C = $random();
		    r_D = $random();
	    end
	    #120 assert(w_S != (r_A + r_B + r_C + r_D)) $fatal("Assertion failed.");
	    $finish();

    end
    endmodule
